/**
 * @package                 {Object Literal}            canvasLab JS Library
 * @author                  'Justin Byrne'              justinbyrne001@gmail.com
 * @link                    'Github'                    https://github.com/justinbyrne001/canvasLab
 * @license                 'MIT License'               https://github.com/justinbyrne001/canvasLab/blob/master/LICENSE.md
 * @version                 {Beta}                      0.1
 */

var canvasLab = {

    /**
     * config                   {Object}                    Nested: object literal variables
     * @var                     {Number} step               Iterator: primarily used to iterate through each color through the colorArray
     * @var                     {Number} lineLength         Line: variable used to incrementally & iteratively increase or decrease shapes during animation
     * @var                     {DOM Element} canvas        Canvas: DOM element
     * @var                     {DOM Element} context       Context: CanvasRenderingContext2D for drawing surface on the <canvas> element
     * @var                     {Object} domWindow          Window: width, height, center x-coordinate, and center y-coordinate
     * @var                     {Object}                    General Information concerning canvasLab
     */
    config : {
        step : 0,
        lineLength : 1,
        canvas : '',
        context : '',
        domWindow : {
            width: window.innerWidth - 18,
            height: window.innerHeight - 4,
            xCenter: (window.innerWidth / 2),
            yCenter: (window.innerHeight / 2),
        },
        about : {
            Author: 'Justin Byrne',
            Created: 'Aug, 17 2014',
            Library: 'canvasLab',
            Updated: 'Sept, 01 2014',
            Version: 0.1,
        }
    },

    /**
     * animateCircleInOut()     {Method}                    Animate: iteratively, generating multiple circles from small to large scale
     * @param                   {Object} el                 Element: 'context'
     * @param                   {Number} degree             Value: of degrees to rotate context during each iteration
     * @param                   {HEX} color                 Color: color value distinguished via either a HEX value, or color keyword (i.e., red, green, blue)
     * @var                     {Number} cx                 X-Coordinate: of the center of the canvas
     * @var                     {Number} cy                 Y-Coordinate: of the center of the canvas
     */
    animateCircleInOut : function(degree,color) {
        var cx = this.config.context.canvas.width/2 + (0.5 * this.config.lineLength);
        var cy = this.config.context.canvas.height/2 + (0.5 * this.config.lineLength);

        // Check: whether @param 'color' is an Array or not
        this.isStrokeArray(color);

        this.config.context.beginPath();
        this.config.context.arc(this.config.context.canvas.width/2+(this.config.lineLength/1.5),this.config.context.canvas.clientWidth/2+(this.config.lineLength/1.5),1*(this.config.lineLength/2),2*Math.PI,false);
        this.config.context.stroke();

        // Rotate: context
        this.rotateContext(cx,cy,degree);

        // Increase: lineLength by 1, post routine
        this.config.lineLength++;
    },

    /**
     * animateTriangleInOut()   {Method}                    Animate: iteratively, generating multiple triangles from small to large scale
     * @param                   {Object} el                 Element: 'context'
     * @param                   {Number} degree             Value: of degrees to rotate context during each iteration
     * @param                   {HEX} color                 Color: color value distinguished via either a HEX value, or color keyword (i.e., red, green, blue)
     * @var                     {Number} x                  X-Coordinate: of the center of the canvas
     * @var                     {Number} y                  Y-Coordinate: of the center of the canvas
     */
    animateTriangleInOut : function(degree,color) {
        // Set: X & Y Coordinate Values for the center of the context
        var x = this.config.context.canvas.width/2;
        var y = this.config.context.canvas.height/2;

        // Check: whether @param 'color' is an Array or not
        this.isStrokeArray(color);

        this.config.context.beginPath();
        this.config.context.moveTo(x+(this.config.lineLength/2),y);
        this.config.context.lineTo(x+this.config.lineLength,y+this.config.lineLength);
        this.config.context.lineTo(x-(this.config.lineLength/this.config.lineLength),y+this.config.lineLength);
        this.config.context.lineTo(x+(this.config.lineLength/2),y);
        this.config.context.stroke();

        // Rotate: context
        this.rotateContext(x,y,degree);

        // Increase: lineLength by 1, post routine
        this.config.lineLength++;
    },

    /**
     * byte2Hex()               {Method}                    Convert: byte numeric value into Hex value
     * @param                   {Number} n                  Number to convert to HEX value
     * @return                  {HEX}                       Return: Hex equivalent of @var 'n'
     */
    byte2Hex : function(n) {
        nybHexString = '0123456789ABCDEF';
        return String( nybHexString.substr( ( n >> 4 ) & 0x0F, 1 ) ) + nybHexString.substr(n & 0x0F, 1);
    },

    /**
     * fillBackground()         {Method}                    Fill: context's full width & height with color value
     * @param                   {HEX/Var} color             Color: value to set background of context
     */
    fillBackground : function(color) {
        this.config.context.rect(0,0,this.config.canvas.width,this.config.canvas.height);
        this.isFillArray(color);
        this.config.context.fill();
    },

    /**
     * getColorGradient()       {Method}                    Get: color gradient array
     * @param                   {Float} redFrequency        Red: frequency at which the this color oscillates
     * @param                   {Float} greenFrequency      Green: frequency at which the this color oscillates
     * @param                   {Float} blueFrequency       Blue: frequency at which the this color oscillates
     * @param                   {Number} alphaPhase         Alpha Phase: first out-of-phase sine wave
     * @param                   {Number} betaPhase          Beta Phase: second out-of-phase sine wave
     * @param                   {Number} gammaPhase         Gama Phase: third out-of-phase sine wave
     * @param                   {Number} center             Center: controls the center of the position of the wave
     * @param                   {Number} width              Width: controls the width of the frequency
     * @param                   {Number} length             Length
     * @return                  {Object}                    ColorArray
     */
    getColorGradient : function(redFrequency,greenFrequency,blueFrequency,alphaPhase,betaPhase,gammaPhase,center,width,length) {
        if (redFrequency == undefined) redFrequency = 0.12;
        if (greenFrequency == undefined) greenFrequency = 0.12;
        if (blueFrequency == undefined) blueFrequency = 0.12;
        if (alphaPhase == undefined) alphaPhase = 0;
        if (betaPhase == undefined) betaPhase = 2;
        if (gammaPhase == undefined) gammaPhase = 4;
        if (center == undefined) center = 128;
        if (width == undefined) width = 127;
        if (length == undefined) length = 50;

        colorArray=[];
        frequency = 0.3;

        for (var i=0; i<length; ++i) {

            red = Math.sin(frequency * i + alphaPhase) * width + center;
            green = Math.sin(frequency * i + betaPhase) * width + center;
            blue = Math.sin(frequency * i + gammaPhase) * width + center;

            result = this.getRgb(red,green,blue);
            colorArray[i] = result;
        }

        return colorArray;
    },

    /**
     * getRgb()                 {Method}                    Get: hexadecimal value through red, green, & blue values
     * @param                   {Number} red                Red: decimal value
     * @param                   {Number} green              Green: decimal value
     * @param                   {Number} blue               Blue: decimal value
     * @return                  {HEX}                       Hex: color value
     */
    getRgb : function(red,green,blue) {
        return '#' + this.byte2Hex(red) + this.byte2Hex(green) + this.byte2Hex(blue);
    },

    /**
     * getWindowDom()           {Method}                    Get: DOM window dimensions, width, height, including X & Y center coordinates
     * @return                  {Object}                    Return: object containing aforementioned dimensional values
     */
    getWindowDom : function() {
        return this.config.domWindow;
    },

    /**
     * isFillArray()            {Method}                    Verify: whether the element passed through is an array or not, then set color value
     * @param                   {Object/Var} el             Element: to test; @uses Array.isArray()
     */
    isFillArray : function(el) {
        if (Array.isArray(el)) {
            this.config.context.fillStyle = el[this.config.step];
            this.config.step++;

            // If: step is greater than length of @var Array, then reset step to zero
            if (this.config.step >= el.length) this.config.step = 0;

        } else {
            this.config.context.fillStyle = el;
        }
    },

    /**
     * isStrokeArray()          {Method}                    Verify: whether the element passed through is an array or not, then set color value
     * @param                   {Object/Var} el             Element: to test; @uses Array.isArray()
     */
    isStrokeArray : function(el) {
        if (Array.isArray(el)) {
            this.config.context.strokeStyle = el[this.config.step];
            this.config.step++;

            // If: step is greater than length of @var Array, then reset step to zero
            if (this.config.step >= el.length) this.config.step = 0;
        } else {
            this.config.context.strokeStyle = el;
        }
    },

    /**
     * output()                 {Method}                    Output: text passed through method
     * @param                   {String} text               Text: to label print prior to output
     * @param                   {Object/Var} output         Output: to be printed on the screen
     * @param                   {Number} i                  Iterator: Line number to print on; begins and upper-left coordinate (x: 10, y: 30)
     */
    output : function(text,output,i) {
        var x = 10;
        var y = i * 30;

        this.config.context.fillStyle = '#000000';
        this.config.context.font = '20px Courier';

        if (typeof text === 'string') {
            this.config.context.fillText(String(text) + ': ' + output,x,y);
        }
    },

    /**
     * rotateContext()          {Method}                    Rotate: context by translating its current X & Y coordinates, then rotating via @var degree, then resetting context to its original state
     * @param                   {Object} el                 Element: 'context'
     * @param                   {Number} x                  Value: to add to x-coordinate
     * @param                   {Number} y                  Value: to add to y-coordinate
     * @param                   {Number} degree             Value: of degrees to rotate context
     */
    rotateContext : function(x,y,degree) {
        this.config.context.translate(x,y);
        this.config.context.rotate((Math.PI/180)*degree);
        this.config.context.translate(-x,-y);
    },

    /**
     * setCanvas()              {Method}                    Set: canvas via document ID, then set canvas's dimensions
     * @param                   {String} id                 ID: document ID of canvas element
     * @param                   {Number} width              Width: numeric value to set canvas
     * @param                   {Number} height             Height: numeric value to set canvas
     */
    setCanvas : function(id,width,height) {
        if (id == undefined) return Modernizr.canvas;

        this.config.canvas = document.getElementById(id);
        this.config.context = this.config.canvas.getContext('2d');

        if (height === undefined) {
            this.config.canvas.width = width;
            this.config.canvas.height = width;
        } else {
            this.config.canvas.width = width;
            this.config.canvas.height = height;
        }
    },

    /**
     * setBorder()              {Method}                    Set: an inset border on the canvas element
     * @param                   {Object} el                 Context: element
     * @param                   {HEX} color                 Color: color value distinguished via either a HEX value, or color keyword (i.e., red, green, blue)
     */
    setBorder : function(color) {
        // Set: canvas width & height
        var width = this.config.canvas.width;
        var height = this.config.canvas.height;

        // Set: color value of strokeStyle()
        // Draw: rectangle
        this.config.context.strokeStyle = color;
        this.config.context.rect(0,0,width,height);
        this.config.context.stroke();
    }

};  // End - canvasLab