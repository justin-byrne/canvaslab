/**
 * @var                         {Method} animate            Store: setInterval() of a specific animation under @function initAnimation()
 * @var                         {Number} ms                 Value: of milliseconds (ms) to iterate with while using @function setInterval()
 */
var animate;
var ms = 70;

/**
 * fillBg()                     {Method}                    Fill: @uses fillBackground() to fill context's background via the color passed through it
 */
function fillBg() {
    canvasLab.fillBackground(colorArray);
    canvasLab.output('color',colorArray[canvasLab.config.step],1);
}

/**
 * circlesInOut()               {Method}                    @uses animateCircleInOut() to generate a series of circles steaming from the center of the canvas to its outer edges
 */
function circlesInOut() {
    canvasLab.animateCircleInOut(20,colorArray);
}

/**
 * trianglesInOut()             {Method}                    @uses animateTriangleInOut() to generate a series of triangles steaming from the center of the canvas to its outer edges
 */
function trianglesInOut() {
    canvasLab.animateTriangleInOut(5,colorArray);
}

/**
 * resetCanvas()                {Method}                    Set: canvas to its default/original state
 */
function resetCanvas() {
    // Clear: interval while also setting @var 'animate' to zero
    clearInterval(animate);
    animate = 0;

    /* @function                (Method) setTransform       Set: context to its default/original state                         */
    canvasLab.config.context.setTransform(1,0,0,1,0,0);
    canvasLab.config.step = 0;
    canvasLab.config.lineLength = 1;
    canvasLab.fillBackground('#5683EB');
}

/**
 * initAnimation()              {Method}                    Initialize: animation type passed through @param; @uses resetFunction() to set canvas's default state
 * @param                       {String} type               Type: of animation to execute
 */
function initAnimation(type) {
    resetCanvas();

    if (type == 'blend') animate = setInterval(fillBg,ms);
    if (type == 'circles') animate = setInterval(circlesInOut,ms);
    if (type == 'triangles') animate = setInterval(trianglesInOut,ms);
}

/**
 * setRangeValue()              {Method}                    Set: each <input> 'range' control & <span> -text element with the passed value for the UI
 * @param                       {Number} value              Value: of <input> control to output to its corresponding DOM element via its corresponding ID
 * @param                       {String} id                 ID: of the DOM element to output/print/display the corresponding value passed through the aforementioned method
 */
function setRangeValue(value,id) {
    var el = [
        document.getElementById(id+'-output'),
        document.getElementById(id+'-text')
    ];

    el[0].value = value;        // <output id="color-freq-output">
    el[1].innerHTML = value;    // <span id="color-freq-text">
}

/**
 * resetColorGradient()         {Method}                    Reset: the UI elements to the original state; @uses resetElements() to reset each DOM UI element
 */
function resetColorGradient() {
    /**
     * resetElements()          {Method}                    Reset: each DOM element via @param elId (element ID) & @param value
     * @param                   {String} elId               Element: identifier in DOM
     * @param                   {Number} value              Value: to reset elements to
     */
    function resetElements(elId,value) {
        document.getElementById(elId).value = value;
        document.getElementById(elId+'-output').value = value;
        document.getElementById(elId+'-text').innerHTML = value;
    }

    resetElements('red-freq',0.12);
    resetElements('green-freq',0.12);
    resetElements('blue-freq',0.12);
    resetElements('alpha-phase',0);
    resetElements('beta-phase',2);
    resetElements('gamma-phase',4);
    resetElements('center',128);
    resetElements('width',127);
    resetElements('length',50);
}

/**
 * setColorGradient()           {Method}                    Set: colorArray using the <input> 'range' DOM element values
 */
function setColorGradient() {
    rF = parseFloat(document.getElementById('red-freq').value);
    gF = parseFloat(document.getElementById('green-freq').value);
    bF = parseFloat(document.getElementById('blue-freq').value);
    aP = parseFloat(document.getElementById('alpha-phase').value);
    bP = parseFloat(document.getElementById('beta-phase').value);
    gP = parseFloat(document.getElementById('gamma-phase').value);
    c = parseFloat(document.getElementById('center').value);
    w = parseFloat(document.getElementById('width').value);
    l = parseFloat(document.getElementById('length').value);

    // colorArray = canvasLab.getColorGradient(0.12,0.12,0.12,0,2,3,128,127,50);
    colorArray = canvasLab.getColorGradient(rF,gF,bF,aP,bP,gP,c,w,l);
}