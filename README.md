<h1>canvasLab</h1>

<p>
canvasLab is an independent & ongoing pet-project
programmed & developed by Justin Byrne. This library is
intended to add additional & simplified functionality to
the HTML5 Canvas element, in an effort to simplify &
promote its use.
</p>

<h1>Live Demo</h1>

<p>
<a href="http://www.byrne-systems.com/canvasLab/">http://www.byrne-systems.com/canvasLab/</a>
</p>

<h2>Functions</h2>

<ul>
	<li>animateCircleInOut()</li>
	<li>animateTriangleInOut()</li>
	<!-- <li>byte2Hex()</li> -->
	<li>fillBackground()</li>
	<li>getColorGradient()</li>
	<li>getRgb()</li>
	<li>getWindowDom()</li>
	<!-- <li>isFillArray()</li> -->
	<!-- <li>isStrokeArray()</li> -->
	<li>output()</li>
	<!-- <li>rotateContext()</li> -->
	<li>setCanvas()</li>
	<li>setBorder()</li>
</ul>

<h2>Usage</h2>

<p>
As previously mentioned, this library is intended to
simplify certain procedures that may be routinely used
with the canvas element, including some more complex
procedures such as animations. Naturally you can cross-blend
canvas's native JavaScript syntax with the syntax used
to access the objects, variables, and functions throughout
this library.
</p>

<p>
Before we begin using the canvas element we need to
first create a DOM node in your HTML document that
we are using, via the following HTML tag:
</p>

```HTML
<canvas></canvas>
```

<h3>setCanvas()</h3>

<p>
Now, normally we would give this tag an identifier (ID),
and then store it within a JavaScript variable, followed
by grabbing the canvas's context through the getContext()
method, and storing that in a variable as well. Albeit,
this commonly only encompasses two lines of code, the
canvasLab library simplifies this process by storing the
context in a variable within the canvasLab object literal,
after it is set. In addition to setting the canvas (through
a single line of code), we can also set the canvas's
dimension (height & width) as well, within the same method
that we use to set the canvas and its context, via the
following method:
</p>

```JavaScript
canvasLab.setCanvas(id,width,height);
```

<p>
Now, before we go setting the canvas element, we need to
ensure that we have a canvas element to set. Therefore,
let us create a canvas element in our HTML document, like
so:
</p>

```HTML
<canvas id="cvs"></canvas>
```

<p>
In addition, we want to ensure that we have our
canvasLab.js library file accessible, and linked someplace
(preferably in either the header or at the end of your
HTML document), using something like:
</p>

```HTML
<script type="text/javascript" src="js/canvasLab.js"></script>
```

<p>
Then, we can set our canvas in one of two ways. The
first method is fairly straightforward, and you only need
to pass the appropriate arguments in order to set the
canvas, store the context in canvasLab, and set the
canvas's dimensions, like so:
</p>

```JavaScript
canvasLab.setCanvas('cvs',700,500);
```
<p>
<strong>Note:</strong> canvasLab is currently constructed
to work in conjunction with Modernizr, to support legacy
browsers. You can obtain a copy of Modernizr either
directly through Modernizr (http://modernizr.com/) or
within this package at Github (https://github.com/justinbyrne001/canvasLab).
In either case, please ensure that you also have Modernizr
referenced within your HTML document, as we did for the
canvasLab library.
</p>

<p>
Now, what the aforementioned method does is it takes our
canvas's ID ('cvs'), stores the canvas element in canvasLab
(to be easily assessable throughout its functions), gets
the 2D context, storing that within canvasLab as well, while
also setting our canvas's dimensions, which in this case
is 700x500. Therefore, your end result should be a canvas
that encompasses a space of 700px in width and 500px in
height, in a single line of code.
</p>

<p>
<strong>Note:</strong> at the moment, canvasLab is 'only'
intended to be utilized with the 2D API context, and there
are currently no future plans to include the 3D API
context, however, that may change in time.
</p>

<p>
In addition, we can use the same line of code (as above)
to set our canvas (as we did), while also setting its
dimensions via a single number variable, squaring our
canvas's dimensions like so:
</p>

```JavaScript
canvasLab.setCanvas('cvs',450);
```

<p>
The result of the above method is pretty much identical
to the first, however, given that it only has one number
variable passed through it, canvasLab automatically squares
our canvas, generating a canvas that is 450x450, or 450px
in width by 450px in height.
</p>

<h3>getWindowDom()</h3>

<p>
Now, assigning a specific dimension to our canvas is nice,
however, what if we wanted our canvas to encompass the entire
Document Object Model (DOM), or window. Well, using canvasLab
we can easily obtain the current dimensions of our browser
by using the function below, which returns the current
windows maximum height and width, including our windows
center X & Y coordinates, as an object, which can be used
to denote the center of our window via the following method:
</p>

```JavaScript
var domWindow = canvasLab.config.getWindowDom();
```

Now that we have our windows dimensions, and center X & Y
coordinates, stored within an object literal called 'domWindow',
we can easily access each of these values using the following
syntax:

```JavaScript
domWindow.width
domWindow.height
domWindow.xCenter
domWindow.yCenter
```

Therefore, if we wanted our canvas to encompass our entire
window, we could do something like this:

```JavaScript
var domWindow = canvasLab.config.getWindowDom();

canvasLab.setCanvas('cvs',domWindow.width,domWindow.height);
```

<p>
Which would cause our canvas to span the entire width &
height of our window. In addition, it is important to note
that each of these values are generated and stored 'after'
the window loads, and will be re-generated if our window
is refreshed.
</p>

<p>
Although future iterations of canvasLab are scheduled to
include a function that will allow a canvas to automatically
resize according to the changing dimensions of a window,
that feature is currently not available. Therefore, it
could be important to maintain a certain level of awareness
of whether or not your web-application is going to need
to be resized post load.
</p>

<h3>fillBackground()</h3>

<p>
Now that we have a canvas element on our screen, lets
set its background color. Normally we would do this by
either defining a CSS style for our canvas's background,
or creating a rectangle to fill the canvas's background
with the color that we intend on utilizing, and then
executing another function to fill that rectangle. Instead,
through canvasLab, we can accomplish this through a single
method, like so:
</p>

```JavaScript
canvasLab.fillBackground(blue);
```

<p>
Which would result in a canvas with a 'blue' background.
It is also important to note that the above method can
also accept hexadecimal color values, variables with a
color value stored in it, or (as shown above) one of the
many color names that are accepted via the canvas's context API.
</p>

<p>
Therefore, the following syntax would absolutely be acceptable:
</p>

```JavaScript
canvasLab.fillBackground('#5683EB');
```

or

```JavaScript
var color = '#5683EB';
canvasLab.fillBackground(color);
```
<h3>setBorder()</h3>

<p>
Sorry, for now our instructions stop here, however, more
instructions are scheduled to come. Please be patient, and
I will have have further instructions available as soon as
I am able.
</p>

<p>
In the meantime, feel free to download canvasLab, play around
with it, and submit any comments, suggestions, or revisions that
you may have, and thank you for your interest.
</p>
